package com.demo.controller;

import com.demo.dao.BasePostRequest;
import com.demo.dao.PersonDaoMybatis;
import com.demo.dao.impl.MyPersonYaml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

/**
 * 基础Base controller 类，用于restfulapi 基类，用于get，post，put，delete
 */
@RestController
@RequestMapping(value = "/base")
public class BaseController {
    @Autowired
    private PersonDaoMybatis personDaoMybatis;

    /**
     * @param id 输入的base id
     * @return
     */
    @GetMapping(value = "/{id}")
    public String getInfo(@PathVariable Integer id) {
        System.out.println("getInfo, id = " + id);
        System.out.println("t_country====>" + t_country);
        System.out.println("t_name====>" + t_name);
        System.out.println("t_likes[0]====>" + t_likes0);
        System.out.println("t_likes[1]====>" + t_likes1);
        System.out.println("t_zjpdir====>" + t_zjpdir);
        System.out.println("=================================");
        System.out.println(env.getProperty("user.name1"));
        System.out.println(env.getProperty("zjpdir"));
        System.out.println(t_personyaml);
        System.out.println(personDaoMybatis.getById(id));
        return "getInfo ..., id=" + id;
    }

    @DeleteMapping(value = "/{id}")
    public String deleteInfo(@PathVariable Integer id) {
        System.out.println("deleteInfo, id = " + id);
        return "deleteInfo ..., id=" + id;
    }

    @PostMapping
    public String updateInfo(@RequestBody BasePostRequest basePostRequest) {
        System.out.println("updateInfo");
        return "updateInfo ...";
    }

    @PutMapping
    public String putInfo(@RequestBody BasePostRequest basePostRequest) {
        System.out.println("putInfo");
        return "putInfo ...";
    }

    /* 读取 yml 中的数据*/
    @Value("${country}")
    private String t_country;

    @Value("${user.name1}")
    private String t_name;

    @Value("${likes[0]}")
    private String t_likes0;

    @Value("${likes[1]}")
    private String t_likes1;

    @Value("${zjpdir}")
    private String t_zjpdir;

    // 读取全部的 yml 中数据，使用自动装配读取
    @Autowired
    private Environment env;

    // 使用自动装配读取
    @Autowired
    private MyPersonYaml t_personyaml;
}
