package com.demo.controller;

import com.demo.dao.PersonDaoMybatisPlus;
import com.demo.dao.PersonDaoMybatis;
import com.demo.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/mysql")
public class BaseMysqlController {
    @Autowired
    private PersonDaoMybatis personDaoMybatis;

    // test MybatisPlus
    @Autowired
    private PersonDaoMybatisPlus personDaoMybatisPlus;

    @GetMapping(value = "/{id}")
    public String getInfo(@PathVariable Integer id) {
        System.out.println("BaseMysqlController getInfo, id ===>" + id);

//        Person p = personDaoMybatis.getById(id);
        Person p = personDaoMybatisPlus.selectById(id);
        if (p != null) {
            return p.toString();
        } else {
            return "{no data}";
        }

//        return personDao.selectById(id).toString();
    }
}
