package com.demo.dao.impl;

import com.demo.dao.PersonYaml;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

// 1. 加载 yml 中的person 数据
// 2. 只有 springboot 中的 bean 才能加载使用，所以要定义spring 管控的 bean
@Component
// 3. 指定加载的属性
@ConfigurationProperties(prefix = "person")
public class MyPersonYaml implements PersonYaml {
    // 数据的名字一定要对应上，类名不是要对应上
    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "MyPerson{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    @Override
    public String save() {
        return "save...";
    }
}
