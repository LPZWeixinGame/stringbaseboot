package com.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.domain.Person;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PersonDaoMybatisPlus extends BaseMapper<Person> {
}
