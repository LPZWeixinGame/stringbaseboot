package com.demo.dao;

import com.demo.domain.Person;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface PersonDaoMybatis {
    @Select("select * from tbl_person where id = #{id}")
    public Person getById(Integer id);
}
