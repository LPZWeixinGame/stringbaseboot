package com.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbasebootApplicationTests {

    // 1. 注入你要测试的对象

    // 2. 执行你要测试的方法

    @Test
    void contextLoads() {
    }

}
